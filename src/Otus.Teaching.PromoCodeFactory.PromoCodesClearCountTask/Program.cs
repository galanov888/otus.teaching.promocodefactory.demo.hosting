using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.ClearPromocodesCountTask
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddScoped(typeof(IRepository<Employee>), (x) => 
                        new InMemoryRepository<Employee>(FakeDataFactory.Employees));
                    services.AddScoped(typeof(IRepository<Role>), (x) => 
                        new InMemoryRepository<Role>(FakeDataFactory.Roles));
                    services.AddHostedService<ClearPromocodesCountHostedService>();
                })
                .UseWindowsService();
    }
}